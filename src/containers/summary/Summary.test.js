import React from "react";
import { render } from "@testing-library/react";
import Summary from "./Summary";
import { Products } from "../../config/products";
import { StoreContext } from "../../context/store-context";

describe("Summary tests", function () {
  const state = {
    checkout: {
      products: Products.map((product) => {
        return { ...product, quantity: 1 };
      }),
      getTotalAmountOfItems: () => 3,
      getTotalWithoutDiscounts: () => 32.5,
      getDiscounts: () => [{ description: "Mug offer", amount: 2.5 }],
      total: () => 30,
    },
    dispatch: () => {},
  };

  it("should render", function () {
    const { getByText } = render(
      <StoreContext.Provider value={{ ...state }}>
        <Summary />
      </StoreContext.Provider>
    );

    expect(getByText("32.5")).toBeInTheDocument();
    expect(getByText("30")).toBeInTheDocument();
    expect(getByText("Mug offer")).toBeInTheDocument();
    expect(getByText("-2.5")).toBeInTheDocument();
  });
});
