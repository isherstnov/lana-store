import React, { useContext } from "react";
import { StoreContext } from "../../context/store-context";
import SummaryInformation from "../../components/summaryInformation/SummaryInformation";

function Summary() {
  const { checkout } = useContext(StoreContext);

  return (
    <SummaryInformation
      discounts={checkout.getDiscounts()}
      totalAmountOfItems={checkout.getTotalAmountOfItems()}
      totalWithoutDiscounts={checkout.getTotalWithoutDiscounts()}
      total={checkout.total()}
    />
  );
}

export default Summary;
