import React, { useContext } from "react";
import * as actionTypes from "../../context/store-actions";
import { StoreContext } from "../../context/store-context";
import CartInformation from "../../components/cartInformation/CartInformation";

function Cart() {
  const { checkout, dispatch } = useContext(StoreContext);

  return (
    <CartInformation
      onIncrement={(productCode) =>
        dispatch({
          type: actionTypes.INCREMENT_PRODUCT_QUANTITY,
          productCode,
        })
      }
      onDecrement={(productCode) =>
        dispatch({
          type: actionTypes.DECREMENT_PRODUCT_QUANTITY,
          productCode,
        })
      }
      onChange={(productCode, quantity) =>
        dispatch({
          type: actionTypes.CHANGE_PRODUCT_QUANTITY,
          productCode,
          quantity,
        })
      }
      products={checkout.products}
    />
  );
}

export default Cart;
