import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Cart from "./Cart";
import { Products } from "../../config/products";
import { StoreContext } from "../../context/store-context";
import * as actionTypes from "../../context/store-actions";

describe("Cart tests", function () {
  const state = {
    checkout: {
      products: Products.map((product) => {
        return { ...product, quantity: 0 };
      }),
    },
    dispatch: () => {},
  };

  it("should render", function () {
    const { getByText } = render(
      <StoreContext.Provider value={{ ...state }}>
        <Cart />
      </StoreContext.Provider>
    );

    expect(getByText("Lana Cap")).toBeInTheDocument();
    expect(getByText("Lana Coffee Mug")).toBeInTheDocument();
    expect(getByText("Lana T-Shirt")).toBeInTheDocument();
  });

  it("should call INCREMENT_PRODUCT_QUANTITY if user adds one", function () {
    const dispatchMock = jest.fn();
    const { getAllByText } = render(
      <StoreContext.Provider value={{ ...state, dispatch: dispatchMock }}>
        <Cart />
      </StoreContext.Provider>
    );

    fireEvent.click(getAllByText("+")[0]);

    expect(dispatchMock).toHaveBeenCalled();
    expect(dispatchMock).toHaveBeenCalledWith({
      type: actionTypes.INCREMENT_PRODUCT_QUANTITY,
      productCode: "CAP",
    });
  });

  it("should call DECREMENT_PRODUCT_QUANTITY if user removes one", function () {
    const dispatchMock = jest.fn();
    const { getAllByText } = render(
      <StoreContext.Provider
        value={{
          ...state,
          checkout: {
            products: Products.map((product) => {
              return { ...product, quantity: 1 };
            }),
          },
          dispatch: dispatchMock,
        }}
      >
        <Cart />
      </StoreContext.Provider>
    );

    fireEvent.click(getAllByText("-")[0]);

    expect(dispatchMock).toHaveBeenCalled();
    expect(dispatchMock).toHaveBeenCalledWith({
      type: actionTypes.DECREMENT_PRODUCT_QUANTITY,
      productCode: "CAP",
    });
  });

  it("should call CHANGE_PRODUCT_QUANTITY if user changes amount", function () {
    const dispatchMock = jest.fn();
    const { getAllByTestId } = render(
      <StoreContext.Provider value={{ ...state, dispatch: dispatchMock }}>
        <Cart />
      </StoreContext.Provider>
    );

    fireEvent.keyDown(getAllByTestId("quantity-amount")[0], { key: 8 });
    fireEvent.change(getAllByTestId("quantity-amount")[0], {
      target: { value: 8 },
    });
    fireEvent.blur(getAllByTestId("quantity-amount")[0]);

    expect(dispatchMock).toHaveBeenCalled();
    expect(dispatchMock).toHaveBeenCalledWith({
      type: actionTypes.CHANGE_PRODUCT_QUANTITY,
      productCode: "CAP",
      quantity: 8,
    });
  });
});
