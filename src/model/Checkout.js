class Checkout {
  #discounts;
  #products;

  constructor(pricingRules) {
    this.#discounts = pricingRules.discounts;
    this.#products = pricingRules.products;
  }

  get products() {
    return this.#products;
  }

  scan(code) {
    let productIndex = this.getProductIndex(code);

    if (productIndex > -1) {
      this.#products[productIndex].quantity++;
      return this;
    }

    this.#products.push({
      code,
      quantity: 1,
    });

    return this;
  }

  unScan(code) {
    let productIndex = this.getProductIndex(code);

    if (productIndex > -1) {
      this.#products[productIndex].quantity--;
    }

    return this;
  }

  changeProductQuantity(code, quantity) {
    let productIndex = this.getProductIndex(code);

    if (productIndex > -1) {
      this.#products[productIndex].quantity = quantity;
      return this;
    }

    this.#products.push({
      code,
      quantity,
    });

    return this;
  }

  getProductIndex(code) {
    return this.#products.findIndex((product) => product.code === code);
  }

  getDiscounts() {
    return this.#products.reduce((discounts, product) => {
      for (const discount of this.#discounts) {
        if (discount.isApplicable(product.code, product.quantity)) {
          discounts.push(discount.getDiscount(product.simpleName, product.quantity, product.price));
        }
      }
      return discounts;
    }, []);
  }

  getTotalAmountOfItems() {
    return this.#products.reduce((accumulator, product) => accumulator + product.quantity, 0);
  }

  getTotalWithoutDiscounts() {
    let total = 0;

    for (const product of this.#products) {
      total += product.price * product.quantity;
    }

    return total;
  }

  total() {
    const totalWithoutDiscounts = this.getTotalWithoutDiscounts();
    const discounts = this.getDiscounts();
    const discountTotalAmount = discounts.reduce((accumulator, discount) => accumulator + discount.amount, 0);

    return totalWithoutDiscounts - discountTotalAmount;
  }
}

export default Checkout;
