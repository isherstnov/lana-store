import "@testing-library/jest-dom/extend-expect";
import Checkout from "./Checkout";
import { Products } from "../config/products";
import { Discounts } from "../config/discounts";

describe("Checkout tests", function () {
  let initialProducts;

  beforeEach(() => {
    initialProducts = Products.map((product) => {
      return { ...product, quantity: 0 };
    });
  });

  describe("Scanning products tests", function () {
    it("should increase a product quantity if it exists already", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      const previousAmountOfProducts = checkout.getTotalAmountOfItems();

      checkout.scan("CAP");

      const currentAmountOfProducts = checkout.getTotalAmountOfItems();
      expect(currentAmountOfProducts).not.toEqual(previousAmountOfProducts);
      expect(checkout.products[0].quantity).toEqual(1);
    });

    it("should add a new product and increase it if the product was not in the list", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      const previousAmountOfProducts = checkout.getTotalAmountOfItems();
      const previousNumberOfDifferentProducts = checkout.products.length;

      checkout.scan("CAP2");

      const currentAmountOfProducts = checkout.getTotalAmountOfItems();
      const currentNumberOfDifferentProducts = checkout.products.length;
      expect(previousAmountOfProducts).not.toEqual(currentAmountOfProducts);
      expect(previousNumberOfDifferentProducts).not.toEqual(currentNumberOfDifferentProducts);
      expect(currentNumberOfDifferentProducts).toEqual(4);
    });

    it("should unscan a product", function () {
      const checkout = new Checkout({
        products: Products.map((product) => {
          return { ...product, quantity: 1 };
        }),
        discounts: Discounts,
      });
      const previousAmountOfProducts = checkout.getTotalAmountOfItems();

      checkout.unScan("CAP");

      const currentAmountOfProducts = checkout.getTotalAmountOfItems();
      expect(currentAmountOfProducts).not.toEqual(previousAmountOfProducts);
      expect(checkout.products[0].quantity).toEqual(0);
    });

    it("should not decrease an non existing product", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      const previousAmountOfProducts = checkout.getTotalAmountOfItems();

      checkout.unScan("CAP2");

      const currentAmountOfProducts = checkout.getTotalAmountOfItems();
      expect(currentAmountOfProducts).toEqual(previousAmountOfProducts);
      expect(checkout.products[0].quantity).toEqual(0);
    });

    it("should change a product quantity", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      const previousAmountOfProducts = checkout.getTotalAmountOfItems();

      checkout.changeProductQuantity("CAP", 10);

      const currentAmountOfProducts = checkout.getTotalAmountOfItems();
      expect(currentAmountOfProducts).not.toEqual(previousAmountOfProducts);
      expect(checkout.products[0].quantity).toEqual(10);
    });

    it("should change a product quantity even if it was not in the list previously", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      const previousAmountOfProducts = checkout.getTotalAmountOfItems();

      checkout.changeProductQuantity("CAP2", 10);

      const currentAmountOfProducts = checkout.getTotalAmountOfItems();
      expect(currentAmountOfProducts).not.toEqual(previousAmountOfProducts);
      expect(currentAmountOfProducts).toEqual(10);
    });
  });

  describe("Getting product index tests", function () {
    it("should return the index of a given product", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });

      const capIndex = checkout.getProductIndex("CAP");
      const mugIndex = checkout.getProductIndex("MUG");
      const shirtIndex = checkout.getProductIndex("SHIRT");

      expect(checkout.products[capIndex].code).toEqual("CAP");
      expect(checkout.products[mugIndex].code).toEqual("MUG");
      expect(checkout.products[shirtIndex].code).toEqual("SHIRT");
    });

    it("should return a negative value if the product sent is not existing in the list", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });

      const nonExistingProductIndex = checkout.getProductIndex("CAP2");

      expect(nonExistingProductIndex).toEqual(-1);
    });
  });

  describe("Calculating discounts tests", function () {
    it("should not return any discount if the products are not applicable", function () {
      const checkout = new Checkout({
        products: Products.map((product) => {
          return { ...product, quantity: 1 };
        }),
        discounts: Discounts,
      });

      const discounts = checkout.getDiscounts();

      expect(discounts.length).toEqual(0);
    });

    it("should return discounts if at least one product has applicable discounts", function () {
      const checkout = new Checkout({
        products: Products.map((product) => {
          return { ...product, quantity: 2 };
        }),
        discounts: Discounts,
      });

      const discounts = checkout.getDiscounts();

      expect(discounts.length).toEqual(1);
      expect(discounts[0].description).toEqual("2x1 cap offer");
      expect(discounts[0].amount).toEqual(5);
    });

    it("should return all available discounts if several products have applicable discounts", function () {
      const checkout = new Checkout({
        products: Products.map((product) => {
          return { ...product, quantity: 3 };
        }),
        discounts: Discounts,
      });

      const discounts = checkout.getDiscounts();

      expect(discounts.length).toEqual(2);
      expect(discounts[0].description).toEqual("2x1 cap offer");
      expect(discounts[1].description).toEqual("x3 shirt offer");
      expect(discounts[0].amount).toEqual(5);
      expect(discounts[1].amount).toEqual(3);
    });
  });

  describe("Calculating total amount of items tests", function () {
    it("should return 0 if there are no products scanned", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });

      const amountOfItems = checkout.getTotalAmountOfItems();

      expect(amountOfItems).toEqual(0);
    });

    it("should return correct value if there are scanned products", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      checkout.scan("CAP").scan("SHIRT");

      const amountOfItems = checkout.getTotalAmountOfItems();

      expect(amountOfItems).toEqual(2);
    });
  });

  describe("Calculating total without discounts tests", function () {
    it("should calculate total without discounts", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      checkout.scan("CAP").scan("CAP");

      const totalWithoutDiscounts = checkout.getTotalWithoutDiscounts();

      const totalWithDiscounts = checkout.total();
      expect(totalWithoutDiscounts).toEqual(10);
      expect(totalWithoutDiscounts).not.toEqual(totalWithDiscounts);
    });
  });

  describe("Calculating total tests", function () {
    it("should include discounts in the total price if there is any", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      checkout.scan("CAP").scan("CAP");

      const totalWithDiscounts = checkout.total();

      const totalWithoutDiscounts = checkout.getTotalWithoutDiscounts();
      expect(totalWithDiscounts).toEqual(5);
      expect(totalWithDiscounts).not.toEqual(totalWithoutDiscounts);
    });

    it("should not include discounts in the total price if there are not discounts available", function () {
      const checkout = new Checkout({
        products: initialProducts,
        discounts: Discounts,
      });
      checkout.scan("MUG").scan("MUG");

      const totalWithDiscounts = checkout.total();

      const totalWithoutDiscounts = checkout.getTotalWithoutDiscounts();
      expect(totalWithDiscounts).toEqual(15);
      expect(totalWithDiscounts).toEqual(totalWithoutDiscounts);
    });
  });
});
