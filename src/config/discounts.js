import { CAP_ID, SHIRT_ID } from "./products";

export const Discounts = [
  {
    name: "2x1",
    isApplicable: (productCode, quantity) => {
      const applicableProducts = [CAP_ID];
      return applicableProducts.includes(productCode) && quantity > 1;
    },
    getDiscount: (productName, quantity, unitPrice) => {
      let amount = 0;
      if (quantity > 1) {
        amount = parseInt(quantity / 2) * unitPrice;
      }
      return {
        description: `2x1 ${productName} offer`,
        amount,
      };
    },
  },
  {
    name: "3x",
    isApplicable: (productCode, quantity) => {
      const applicableProducts = [SHIRT_ID];
      return applicableProducts.includes(productCode) && quantity > 2;
    },
    getDiscount: (productName, quantity, unitPrice) => {
      let amount = 0;
      if (quantity > 2) {
        amount = quantity * unitPrice * 0.05;
      }
      return {
        description: `x3 ${productName} offer`,
        amount,
      };
    },
  },
];
