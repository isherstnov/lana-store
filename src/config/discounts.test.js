import { Discounts } from "./discounts";
import { CAP_ID, MUG_ID, SHIRT_ID } from "./products";

describe("Discounts tests", function () {
  describe("2x1 Tests", function () {
    const discount = Discounts[0];

    it("should say that is not applicable for MUG or SHIRT products with any amount of products", function () {
      expect(discount.isApplicable(MUG_ID, 1)).toBe(false);
      expect(discount.isApplicable(MUG_ID, 2)).toBe(false);
      expect(discount.isApplicable(MUG_ID, 50)).toBe(false);
      expect(discount.isApplicable(SHIRT_ID, 1)).toBe(false);
      expect(discount.isApplicable(SHIRT_ID, 2)).toBe(false);
      expect(discount.isApplicable(SHIRT_ID, 50)).toBe(false);
    });

    it("should say that is applicable for CAP with 2 or more of quantity", function () {
      expect(discount.isApplicable(CAP_ID, 2)).toBe(true);
      expect(discount.isApplicable(CAP_ID, 50)).toBe(true);
    });

    it("should say that is not applicable for CAP with 1 or less quantity", function () {
      expect(discount.isApplicable(CAP_ID, 1)).toBe(false);
      expect(discount.isApplicable(CAP_ID, 0)).toBe(false);
    });

    it("should say that the discount is 0 if there are less than 2 products", function () {
      expect(discount.getDiscount(MUG_ID, 1, 5)).toEqual({
        description: "2x1 MUG offer",
        amount: 0,
      });
      expect(discount.getDiscount(MUG_ID, 0, 5)).toEqual({
        description: "2x1 MUG offer",
        amount: 0,
      });
    });

    it("should calculate the discount properly", function () {
      expect(discount.getDiscount(MUG_ID, 2, 5)).toEqual({
        description: "2x1 MUG offer",
        amount: 5,
      });
      expect(discount.getDiscount(MUG_ID, 3, 5)).toEqual({
        description: "2x1 MUG offer",
        amount: 5,
      });
      expect(discount.getDiscount(MUG_ID, 4, 5)).toEqual({
        description: "2x1 MUG offer",
        amount: 10,
      });
    });
  });

  describe("3x Tests", function () {
    const discount = Discounts[1];

    it("should say that is not applicable for CAP or MUG products with any amount of products", function () {
      expect(discount.isApplicable(CAP_ID, 1)).toBe(false);
      expect(discount.isApplicable(CAP_ID, 2)).toBe(false);
      expect(discount.isApplicable(CAP_ID, 50)).toBe(false);
      expect(discount.isApplicable(MUG_ID, 1)).toBe(false);
      expect(discount.isApplicable(MUG_ID, 2)).toBe(false);
      expect(discount.isApplicable(MUG_ID, 50)).toBe(false);
    });

    it("should say that is applicable for SHIRT with 3 or more of quantity", function () {
      expect(discount.isApplicable(SHIRT_ID, 3)).toBe(true);
      expect(discount.isApplicable(SHIRT_ID, 50)).toBe(true);
    });

    it("should say that is not applicable for SHIRT with 2 or less quantity", function () {
      expect(discount.isApplicable(SHIRT_ID, 2)).toBe(false);
      expect(discount.isApplicable(SHIRT_ID, 1)).toBe(false);
      expect(discount.isApplicable(SHIRT_ID, 0)).toBe(false);
    });

    it("should say that the discount is 0 if there are less than 3 products", function () {
      expect(discount.getDiscount(SHIRT_ID, 2, 20)).toEqual({
        description: "x3 SHIRT offer",
        amount: 0,
      });
      expect(discount.getDiscount(SHIRT_ID, 1, 20)).toEqual({
        description: "x3 SHIRT offer",
        amount: 0,
      });
      expect(discount.getDiscount(SHIRT_ID, 0, 20)).toEqual({
        description: "x3 SHIRT offer",
        amount: 0,
      });
    });

    it("should calculate the discount properly", function () {
      expect(discount.getDiscount(SHIRT_ID, 3, 20)).toEqual({
        description: "x3 SHIRT offer",
        amount: 3,
      });
      expect(discount.getDiscount(SHIRT_ID, 4, 20)).toEqual({
        description: "x3 SHIRT offer",
        amount: 4,
      });
      expect(discount.getDiscount(SHIRT_ID, 5, 20)).toEqual({
        description: "x3 SHIRT offer",
        amount: 5,
      });
    });
  });
});
