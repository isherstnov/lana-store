import shirtImg from "../assets/products/shirt.png";
import mugImg from "../assets/products/mug.png";
import capImg from "../assets/products/cap.png";
import detailedShirtImg from "../assets/products/detailed/shirt.png";
import detailedMugImg from "../assets/products/detailed/mug.png";
import detailedCapImg from "../assets/products/detailed/cap.png";

export const CAP_ID = "CAP";
export const SHIRT_ID = "SHIRT";
export const MUG_ID = "MUG";

export const Products = [
  {
    code: CAP_ID,
    name: "Lana Cap",
    price: 5,
    simpleName: "cap",
    img: capImg,
    detailedInformation: {
      img: detailedCapImg,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sodales semper edit sit amet interdum. Praesent volutpat sed elit vel consectetur. Nulla tempus tincidunt ex, sit amet semper ipsum imperdiet varius. In rutrum aliquam nisl, sagittis faucibus felis bibendum id.",
    },
  },
  {
    code: SHIRT_ID,
    name: "Lana T-Shirt",
    price: 20,
    simpleName: "shirt",
    img: shirtImg,
    detailedInformation: {
      img: detailedShirtImg,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sodales semper edit sit amet interdum. Praesent volutpat sed elit vel consectetur. Nulla tempus tincidunt ex, sit amet semper ipsum imperdiet varius. In rutrum aliquam nisl, sagittis faucibus felis bibendum id.",
    },
  },
  {
    code: MUG_ID,
    name: "Lana Coffee Mug",
    price: 7.5,
    simpleName: "mug",
    img: mugImg,
    detailedInformation: {
      img: detailedMugImg,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sodales semper edit sit amet interdum. Praesent volutpat sed elit vel consectetur. Nulla tempus tincidunt ex, sit amet semper ipsum imperdiet varius. In rutrum aliquam nisl, sagittis faucibus felis bibendum id.",
    },
  },
];
