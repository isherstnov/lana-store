import React from "react";
import PropTypes from "prop-types";
import "./ProductModal.scss";
import Button from "../button/Button";
import Modal from "../modal/Modal";

function ProductModal(props) {
  const { imgSrc, name, price, description, code, onAddToCart, onClose, showModal } = props;

  return (
    <Modal onClose={onClose} show={showModal}>
      <div className="product-modal">
        <div className="product-modal-img-container">
          <img src={imgSrc} alt={name} />
        </div>
        <div className="product-modal-aside">
          <div className="product-modal-aside-container">
            <div className="product-modal-aside-title">
              <h1>{name}</h1>

              <strong>
                <span className="price">{price}</span>
                <span className="price-currency">€</span>
              </strong>
            </div>

            <span className="product-modal-aside-description">{description}</span>

            <p className="product-modal-aside-code">Product code: {code}</p>

            <Button onClick={onAddToCart} className="primary">
              Add to cart
            </Button>
          </div>
        </div>
      </div>
    </Modal>
  );
}

ProductModal.defaultProps = {
  imgSrc: "",
  name: "",
  price: 0,
  description: "",
  code: "",
  onAddToCart: () => {},
  showModal: false,
  onClose: () => {},
};

ProductModal.propTypes = {
  imgSrc: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  description: PropTypes.string,
  code: PropTypes.string,
  onAddToCart: PropTypes.func,
  showModal: PropTypes.bool,
  onClose: PropTypes.func,
};

export default ProductModal;
