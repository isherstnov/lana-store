import React from "react";
import { fireEvent, render } from "@testing-library/react";
import ProductModal from "./ProductModal";

describe("ProductModal tests", function () {
  const props = {
    imgSrc: "",
    name: "NAME",
    price: 5,
    description: "DESCRIPTION",
    code: "INTERNAL CODE",
    showModal: true,
    onClose: () => {},
    onAddToCart: () => {},
  };

  it("should render", function () {
    const { getByText } = render(<ProductModal {...props} />);

    expect(getByText("NAME")).toBeInTheDocument();
    expect(getByText("DESCRIPTION")).toBeInTheDocument();
    expect(getByText("5")).toBeInTheDocument();
    expect(getByText("Product code: INTERNAL CODE")).toBeInTheDocument();
    expect(getByText("Add to cart")).toBeInTheDocument();
  });

  it("should trigger onAddToCart if click is performed in button 'Add to cart'", function () {
    const onAddToCartHandler = jest.fn();
    const { getByText } = render(<ProductModal {...props} onAddToCart={onAddToCartHandler} />);

    fireEvent.click(getByText("Add to cart"));

    expect(onAddToCartHandler).toHaveBeenCalled();
  });

  it("should show modal if showModal is set to true", function () {
    const { getByText } = render(<ProductModal {...props} showModal={true} />);

    expect(getByText("DESCRIPTION")).toBeInTheDocument();
    expect(getByText("Product code: INTERNAL CODE")).toBeInTheDocument();
    expect(getByText("Add to cart")).toBeInTheDocument();
  });

  it("should not show modal if showModal is set to true", function () {
    const { queryByText } = render(<ProductModal {...props} showModal={false} />);

    expect(queryByText("DESCRIPTION")).not.toBeInTheDocument();
    expect(queryByText("Product code: INTERNAL CODE")).not.toBeInTheDocument();
    expect(queryByText("Add to cart")).not.toBeInTheDocument();
  });
});
