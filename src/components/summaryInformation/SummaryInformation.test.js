import React from "react";
import { render } from "@testing-library/react";
import SummaryInformation from "./SummaryInformation";

describe("SummaryInformation tests", function () {
  const props = {
    discounts: [{ description: "Mug offer", amount: 2.5 }],
    totalAmountOfItems: 3,
    totalWithoutDiscounts: 32.5,
    total: 30,
  };

  it("should render", function () {
    const { getByText } = render(<SummaryInformation {...props} />);

    expect(getByText("Order Summary")).toBeInTheDocument();
    expect(getByText("3 Item(s)")).toBeInTheDocument();
    expect(getByText("Discounts")).toBeInTheDocument();
    expect(getByText("Total cost")).toBeInTheDocument();
    expect(getByText("Checkout")).toBeInTheDocument();
    expect(getByText("32.5")).toBeInTheDocument();
    expect(getByText("30")).toBeInTheDocument();
    expect(getByText("Mug offer")).toBeInTheDocument();
    expect(getByText("-2.5")).toBeInTheDocument();
  });
});
