import React from "react";
import "./SummaryInformation.scss";
import Button from "../button/Button";
import PropTypes from "prop-types";

function SummaryInformation(props) {
  const { discounts, totalAmountOfItems, totalWithoutDiscounts, total } = props;

  const getDiscounts = () => {
    return discounts.map((discount) => {
      return (
        <li key={discount.description}>
          <span>{discount.description}</span>

          <strong>
            <span className="price">-{discount.amount}</span>
            <span className="price-currency">€</span>
          </strong>
        </li>
      );
    });
  };

  return (
    <aside className="summary">
      <h1 className="summary-title">Order Summary</h1>

      <ul className="summary-items">
        <li>
          <span>{totalAmountOfItems} Item(s)</span>

          <strong>
            <span className="price">{totalWithoutDiscounts}</span>
            <span className="price-currency">€</span>
          </strong>
        </li>
      </ul>
      <div className="summary-discounts">
        <h2>Discounts</h2>
        <ul>{getDiscounts()}</ul>
      </div>
      <div className="summary-total">
        <ul>
          <li>
            <span className="summary-total-cost">Total cost</span>

            <strong>
              <span className="price summary-total-price">{total}</span>
              <span className="price-currency summary-total-price">€</span>
            </strong>
          </li>
        </ul>
        <Button className="primary">Checkout</Button>
      </div>
    </aside>
  );
}

SummaryInformation.defaultProps = {
  discounts: [],
  totalAmountOfItems: 0,
  totalWithoutDiscounts: 0,
  total: 0,
};

SummaryInformation.propTypes = {
  discounts: PropTypes.arrayOf(PropTypes.exact({ description: PropTypes.string, amount: PropTypes.number })),
  totalAmountOfItems: PropTypes.number,
  totalWithoutDiscounts: PropTypes.number,
  total: PropTypes.number,
};

export default SummaryInformation;
