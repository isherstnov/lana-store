import React from "react";
import PropTypes from "prop-types";
import "./Modal.scss";

const Modal = (props) => {
  if (!props.show) {
    return null;
  }

  return (
    <div className="modal">
      <div className="modal-content">{props.children}</div>
      <button
        data-testid="modal-toggle-button"
        className="modal-toggle-button"
        onClick={(event) => props.onClose(event)}
      >
        X
      </button>
    </div>
  );
};

Modal.defaultProps = {
  show: false,
  children: null,
  onClose: () => {},
};

Modal.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node,
  onClose: PropTypes.func,
};

export default Modal;
