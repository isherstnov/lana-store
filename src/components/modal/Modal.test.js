import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Modal from "./Modal";

describe("Modal tests", function () {
  const props = {
    children: <span>Modal content</span>,
    show: true,
    onClose: () => {},
  };

  it("should render", function () {
    const { getByTestId, getByText } = render(<Modal {...props} />);

    expect(getByText("Modal content")).toBeInTheDocument();
    expect(getByTestId("modal-toggle-button")).toBeInTheDocument();
  });

  it("should not render if show is sent to false", function () {
    const { queryByTestId, queryByText } = render(<Modal {...Object.assign({}, props, { show: false })} />);

    expect(queryByText("Modal content")).toBeNull();
    expect(queryByTestId("modal-toggle-button")).toBeNull();
  });

  it("should trigger onClose if click is performed in toggle button", function () {
    const clickHandler = jest.fn();
    const { getByTestId } = render(<Modal {...props} onClose={clickHandler} />);

    fireEvent.click(getByTestId("modal-toggle-button"));

    expect(clickHandler).toHaveBeenCalled();
  });
});
