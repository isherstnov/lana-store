import React, { useState } from "react";
import PropTypes from "prop-types";
import "./Product.scss";
import QuantitySelector from "../quantitySelector/QuantitySelector";
import ProductModal from "../productModal/ProductModal";

function Product(props) {
  const [showModal, setShowModal] = useState(false);

  const { code, name, price, amount, imgSrc, imgAlt, detailedInformation, onIncrement, onDecrement, onChange } = props;
  const detailedViewAvailable = detailedInformation && detailedInformation.img && detailedInformation.description;

  const onIncrementQuantity = () => {
    onIncrement(code);
  };

  const onDecrementQuantity = () => {
    onDecrement(code);
  };

  const onQuantityChange = (quantity) => {
    onChange(code, quantity);
  };

  const toggleModal = () => {
    if (detailedViewAvailable) {
      setShowModal(!showModal);
    }
  };

  const onAddToCart = () => {
    onIncrement(code);
    toggleModal();
  };

  const getModal = () => {
    if (!detailedViewAvailable) {
      return null;
    }

    return (
      <ProductModal
        onClose={toggleModal}
        showModal={showModal}
        imgSrc={detailedInformation.img}
        name={name}
        price={price}
        description={detailedInformation.description}
        code={code}
        onAddToCart={onAddToCart}
      />
    );
  };

  return (
    <React.Fragment>
      <div className="col-product">
        <figure
          data-testid="col-product-image"
          className={`col-product-image ${detailedViewAvailable ? "col-product-image-details-available" : ""}`}
          onClick={toggleModal}
        >
          <img src={imgSrc} alt={imgAlt} />
          <div>
            <h3 className="col-product-image-title">{name}</h3>

            <p>Product code {code}</p>
          </div>
        </figure>
      </div>
      {getModal()}
      <div className="col-quantity">
        <QuantitySelector
          key={`${code}-QuantitySelector-${amount}`}
          amount={amount}
          min={0}
          max={99}
          onChange={onQuantityChange}
          onDecrement={onDecrementQuantity}
          onIncrement={onIncrementQuantity}
        />
      </div>
      <div className="col-price">
        <span className="price">{price}</span>
        <span className="product-currency">€</span>
      </div>
      <div className="col-total">
        <span className="price">{price * amount}</span>
        <span className="price-currency">€</span>
      </div>
    </React.Fragment>
  );
}

Product.defaultProps = {
  code: "",
  name: "",
  price: 0,
  amount: 0,
  imgSrc: "",
  imgAlt: "",
  detailedInformation: null,
  onIncrement: () => {},
  onDecrement: () => {},
  onChange: () => {},
};

Product.propTypes = {
  code: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  amount: PropTypes.number,
  imgSrc: PropTypes.string,
  imgAlt: PropTypes.string,
  detailedInformation: PropTypes.exact({
    img: PropTypes.string,
    description: PropTypes.string,
  }),
  onIncrement: PropTypes.func,
  onDecrement: PropTypes.func,
  onChange: PropTypes.func,
};

export default Product;
