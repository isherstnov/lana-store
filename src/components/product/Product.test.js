import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Product from "./Product";

describe("Product tests", function () {
  const props = {
    code: "CODE",
    name: "NAME",
    price: 5,
    amount: 2,
    imgSrc: "",
    imgAlt: "IMG ALT",
    onIncrement: () => {},
    onDecrement: () => {},
    onChange: () => {},
  };

  it("should render", function () {
    const { getByText } = render(<Product {...props} />);

    expect(getByText("NAME")).toBeInTheDocument();
    expect(getByText("Product code CODE")).toBeInTheDocument();
    expect(getByText("5")).toBeInTheDocument();
    expect(getByText("10")).toBeInTheDocument();
  });

  it("should add a class of detailed information available if this information is sent", function () {
    const { getByTestId } = render(
      <Product {...props} detailedInformation={{ description: "Description", img: "imgsrc" }} />
    );

    const element = getByTestId("col-product-image");
    expect(element).toBeInTheDocument();
    expect(element.classList[1]).toEqual("col-product-image-details-available");
  });

  it("should render a modal if detailed information is sent and click is performed in the name product", function () {
    const { getByTestId, getByText } = render(
      <Product
        {...props}
        detailedInformation={{
          description: "Detailed description only visible in modal",
          img: "imgsrc",
        }}
      />
    );
    const productNameContainer = getByTestId("col-product-image");

    fireEvent.click(productNameContainer);

    expect(getByText("Detailed description only visible in modal")).toBeInTheDocument();
  });

  it("should close the modal if it was open and the user clicks on X to close it", function () {
    const { getByTestId, queryByText } = render(
      <Product
        {...props}
        detailedInformation={{
          description: "Detailed description only visible in modal",
          img: "imgsrc",
        }}
      />
    );
    const productNameContainer = getByTestId("col-product-image");
    fireEvent.click(productNameContainer);

    fireEvent.click(getByTestId("modal-toggle-button"));

    expect(queryByText("Detailed description only visible in modal")).toBeNull();
  });

  it("should close the modal and trigger onIncrement if it was open and the user clicks on add to cart", function () {
    const onIncrementHandler = jest.fn();
    const { getByTestId, queryByText, getByText } = render(
      <Product
        {...props}
        detailedInformation={{
          description: "Detailed description only visible in modal",
          img: "imgsrc",
        }}
        onIncrement={onIncrementHandler}
      />
    );
    const productNameContainer = getByTestId("col-product-image");
    fireEvent.click(productNameContainer);

    fireEvent.click(getByText("Add to cart"));

    expect(queryByText("Detailed description only visible in modal")).toBeNull();
    expect(onIncrementHandler).toHaveBeenCalled();
  });

  it("should trigger onIncrement if a click on plus is triggered", function () {
    const onIncrementHandler = jest.fn();
    const { getByText } = render(<Product {...props} onIncrement={onIncrementHandler} />);

    fireEvent.click(getByText("+"));

    expect(onIncrementHandler).toHaveBeenCalled();
  });

  it("should trigger onDecrement if a click on minus is triggered", function () {
    const onDecrementHandler = jest.fn();
    const { getByText } = render(<Product {...props} onDecrement={onDecrementHandler} />);
    fireEvent.click(getByText("+"));

    fireEvent.click(getByText("-"));

    expect(onDecrementHandler).toHaveBeenCalled();
  });

  it("should trigger onChange if a change on quantity selector is triggered", function () {
    const onChange = jest.fn();
    const { getByTestId } = render(<Product {...props} onChange={onChange} />);

    fireEvent.change(getByTestId("quantity-amount"), { target: { value: 10 } });
    fireEvent.blur(getByTestId("quantity-amount"));

    expect(onChange).toHaveBeenCalled();
  });
});
