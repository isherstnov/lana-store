import React from "react";
import PropTypes from "prop-types";
import "./Button.scss";

function Button(props) {
  return (
    <button onClick={props.onClick} className={`button ${props.className}`}>
      {props.children}
    </button>
  );
}

Button.defaultProps = {
  onClick: () => {},
  className: "",
};

Button.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
};

export default Button;
