import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Button from "./Button";

describe("Button tests", function () {
  const props = {
    children: "Click me",
  };

  it("should render", function () {
    const { getByText } = render(<Button {...props} className="primary" />);

    const element = getByText("Click me");
    expect(element).toBeInTheDocument();
    expect(element.classList[1]).toEqual("primary");
  });

  it("should trigger onClick if click is performed", function () {
    const clickHandler = jest.fn();
    const { getByText } = render(<Button {...props} onClick={clickHandler} />);

    fireEvent.click(getByText("Click me"));

    expect(clickHandler).toHaveBeenCalled();
  });
});
