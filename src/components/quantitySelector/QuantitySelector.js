import React, { useState } from "react";
import PropTypes from "prop-types";
import "./QuantitySelector.scss";

function QuantitySelector(props) {
  const [inputValue, setInputValue] = useState(props.amount);

  const onChange = (event) => {
    setInputValue(event.target.value);
  };

  const onKeyDown = (event) => {
    const isArrows = event.key === "ArrowLeft" || event.key === "ArrowRight";
    const isNumber = /^[0-9]$/.test(event.key);
    const isDelete = event.key === "Delete" || event.key === "Backspace";

    if (!isArrows && !isDelete && !isNumber) {
      event.preventDefault();
    }
  };

  const onBlur = () => {
    const { onChange, max, min } = props;

    if (inputValue === "") {
      onChange(min);
      return;
    }

    let correctValue = Number.isInteger(inputValue) ? inputValue : parseInt(inputValue);

    if (correctValue > max) {
      correctValue = max;
    } else if (correctValue < min) {
      correctValue = min;
    }

    setInputValue(correctValue);
    onChange(correctValue);
  };

  const onDecrement = () => {
    if (inputValue - 1 < props.min) {
      return;
    }
    props.onDecrement();
  };

  const onIncrement = () => {
    if (inputValue + 1 > props.max) {
      return;
    }
    props.onIncrement();
  };

  return (
    <React.Fragment>
      <button onClick={onDecrement} className="quantity-count">
        -
      </button>
      <input
        data-testid="quantity-amount"
        inputMode="numeric"
        type="number"
        onBlur={onBlur}
        onKeyDown={onKeyDown}
        onChange={onChange}
        min={props.min}
        max={props.max}
        className="quantity-amount"
        value={inputValue}
      />
      <button onClick={onIncrement} className="quantity-count">
        +
      </button>
    </React.Fragment>
  );
}

QuantitySelector.defaultProps = {
  amount: 0,
  max: 99,
  min: 0,
  onChange: () => {},
  onDecrement: () => {},
  onIncrement: () => {},
};

QuantitySelector.propTypes = {
  amount: PropTypes.number,
  max: PropTypes.number,
  min: PropTypes.number,
  onChange: PropTypes.func,
  onDecrement: PropTypes.func,
  onIncrement: PropTypes.func,
};

export default QuantitySelector;
