import React from "react";
import { fireEvent, render } from "@testing-library/react";
import QuantitySelector from "./QuantitySelector";

describe("QuantitySelector tests", function () {
  const props = {
    amount: 5,
    onChange: () => {},
  };

  it("should render", function () {
    const { getByText, getByTestId } = render(<QuantitySelector {...props} />);

    expect(getByText("-")).toBeInTheDocument();
    expect(getByText("+")).toBeInTheDocument();
    expect(getByTestId("quantity-amount")).toBeInTheDocument();
    expect(getByTestId("quantity-amount").value).toEqual("5");
  });

  it("should trigger onDecrement if minus button is clicked and the new value is above the minimum", function () {
    const onDecrementHandler = jest.fn();
    const { getByText } = render(<QuantitySelector {...props} onDecrement={onDecrementHandler} min={1} />);

    fireEvent.click(getByText("-"));

    expect(onDecrementHandler).toHaveBeenCalled();
  });

  it("should not trigger onDecrement if minus button is clicked and the new value is below the minimum", function () {
    const onDecrementHandler = jest.fn();
    const { getByText } = render(<QuantitySelector {...props} onDecrement={onDecrementHandler} min={5} />);

    fireEvent.click(getByText("-"));

    expect(onDecrementHandler).not.toHaveBeenCalled();
  });

  it("should trigger onIncrement if plus button is clicked and the new value is below the maximum", function () {
    const onIncrementHandler = jest.fn();
    const { getByText } = render(<QuantitySelector {...props} onIncrement={onIncrementHandler} max={10} />);

    fireEvent.click(getByText("+"));

    expect(onIncrementHandler).toHaveBeenCalled();
  });

  it("should not trigger onIncrement if plus button is clicked and the new value is above the maximum", function () {
    const onIncrementHandler = jest.fn();
    const { getByText } = render(<QuantitySelector {...props} onIncrement={onIncrementHandler} max={5} />);

    fireEvent.click(getByText("+"));

    expect(onIncrementHandler).not.toHaveBeenCalled();
  });

  it("should not allow entering any key besides arrows, numbers and deletion", function () {
    const { getByTestId } = render(<QuantitySelector {...props} max={5} />);

    fireEvent.keyDown(getByTestId("quantity-amount"), { key: "a" });
    fireEvent.keyDown(getByTestId("quantity-amount"), { key: "%" });
    fireEvent.keyDown(getByTestId("quantity-amount"), { key: '-"' });
    fireEvent.blur(getByTestId("quantity-amount"));

    expect(getByTestId("quantity-amount").value).toEqual("5");
  });

  it("should not allow to enter a number less than the minimum", function () {
    const { getByTestId } = render(<QuantitySelector {...props} min={4} />);

    fireEvent.keyDown(getByTestId("quantity-amount"), { key: 3 });
    fireEvent.change(getByTestId("quantity-amount"), { target: { value: 3 } });
    fireEvent.blur(getByTestId("quantity-amount"));

    expect(getByTestId("quantity-amount").value).toEqual("4");
  });

  it("should not allow to enter a number bigger than the maximum", function () {
    const { getByTestId } = render(<QuantitySelector {...props} max={7} />);

    fireEvent.keyDown(getByTestId("quantity-amount"), { key: 8 });
    fireEvent.change(getByTestId("quantity-amount"), { target: { value: 8 } });
    fireEvent.blur(getByTestId("quantity-amount"));

    expect(getByTestId("quantity-amount").value).toEqual("7");
  });

  it("should allow to enter a number that is less than the max and bigger than the min", function () {
    const { getByTestId } = render(<QuantitySelector {...props} min={1} max={10} />);

    fireEvent.keyDown(getByTestId("quantity-amount"), { key: 8 });
    fireEvent.change(getByTestId("quantity-amount"), { target: { value: 8 } });
    fireEvent.blur(getByTestId("quantity-amount"));

    expect(getByTestId("quantity-amount").value).toEqual("8");
  });

  it("should put the min number if the value entered is empty", function () {
    const onChangeHandler = jest.fn();
    const { getByTestId } = render(<QuantitySelector {...props} min={1} max={10} onChange={onChangeHandler} />);

    fireEvent.change(getByTestId("quantity-amount"), { target: { value: "" } });
    fireEvent.blur(getByTestId("quantity-amount"));

    expect(onChangeHandler).toHaveBeenCalled();
    expect(onChangeHandler).toHaveBeenCalledWith(1);
  });
});
