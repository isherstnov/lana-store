import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

describe("App tests", function () {
  it("should render", function () {
    const { getByTestId } = render(<App />);

    expect(getByTestId("app-container")).toBeInTheDocument();
  });
});
