import React from "react";
import "./App.scss";
import Cart from "../../containers/cart/Cart";
import Summary from "../../containers/summary/Summary";

function App() {
  return (
    <div className="app">
      <div className="app-container" data-testid="app-container">
        <Cart />
        <Summary />
      </div>
    </div>
  );
}

export default App;
