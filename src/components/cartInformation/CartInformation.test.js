import React from "react";
import { render } from "@testing-library/react";
import CartInformation from "./CartInformation";
import { Products } from "../../config/products";

describe("CartInformation tests", function () {
  const props = {
    products: Products.map((product) => {
      return { ...product, quantity: 0 };
    }),
    onIncrement: () => {},
    onDecrement: () => {},
    onChange: () => {},
  };

  it("should render", function () {
    const { getByText } = render(<CartInformation {...props} />);

    expect(getByText("Shopping cart")).toBeInTheDocument();
    expect(getByText("Product details")).toBeInTheDocument();
    expect(getByText("Quantity")).toBeInTheDocument();
    expect(getByText("Price")).toBeInTheDocument();
    expect(getByText("Total")).toBeInTheDocument();
    expect(getByText("Lana Cap")).toBeInTheDocument();
    expect(getByText("Lana Coffee Mug")).toBeInTheDocument();
    expect(getByText("Lana T-Shirt")).toBeInTheDocument();
  });
});
