import React from "react";
import "./CartInformation.scss";
import PropTypes from "prop-types";
import Product from "../product/Product";

function CartInformation(props) {
  const { products, onIncrement, onDecrement, onChange } = props;

  const buildProduct = (product) => {
    return (
      <Product
        imgSrc={product.img}
        imgAlt={product.simpleName}
        name={product.name}
        code={product.code}
        price={product.price}
        amount={product.quantity}
        detailedInformation={product.detailedInformation}
        onIncrement={onIncrement}
        onDecrement={onDecrement}
        onChange={onChange}
      />
    );
  };

  const getProducts = () => products.map((product) => <li key={product.code}>{buildProduct(product)}</li>);

  return (
    <section className="cart">
      <h1 className="cart-title">Shopping cart</h1>

      <ul className="cart-table-head">
        <li>
          <span className="cart-col-product table-head">Product details</span>
          <span className="cart-col-quantity table-head">Quantity</span>
          <span className="cart-col-price table-head">Price</span>
          <span className="cart-col-total table-head">Total</span>
        </li>
      </ul>
      <ul className="cart-products">{getProducts()}</ul>
    </section>
  );
}

CartInformation.defaultProps = {
  products: [],
  onIncrement: () => {},
  onDecrement: () => {},
  onChange: () => {},
};

CartInformation.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.exact({
      code: PropTypes.string,
      name: PropTypes.string,
      price: PropTypes.number,
      quantity: PropTypes.number,
      simpleName: PropTypes.string,
      img: PropTypes.string,
      detailedInformation: PropTypes.exact({
        img: PropTypes.string,
        description: PropTypes.string,
      }),
    })
  ),
  onIncrement: PropTypes.func,
  onDecrement: PropTypes.func,
  onChange: PropTypes.func,
};

export default CartInformation;
