import React, { useReducer } from "react";
import { Products } from "../config/products";
import { Discounts } from "../config/discounts";
import storeReducer from "./store-reducer";
import Checkout from "../model/Checkout";

const initialProducts = Products.map((product) => {
  return { ...product, quantity: 0 };
});

const initialState = {
  checkout: new Checkout({ products: initialProducts, discounts: Discounts }),
};

export const StoreContext = React.createContext(initialState);

const StoreContextProvider = (props) => {
  const [state, dispatch] = useReducer(storeReducer, initialState);

  return <StoreContext.Provider value={{ ...state, dispatch }}>{props.children}</StoreContext.Provider>;
};

export default StoreContextProvider;
