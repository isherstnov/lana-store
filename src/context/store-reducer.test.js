import storeReducer from "./store-reducer";
import * as actionTypes from "./store-actions";

describe("store-reducer tests", function () {
  it("should scan product if INCREMENT_PRODUCT_QUANTITY is sent as action", function () {
    const productCode = "CAP";
    const scanSpy = jest.fn();

    storeReducer({ checkout: { scan: scanSpy } }, { type: actionTypes.INCREMENT_PRODUCT_QUANTITY, productCode });

    expect(scanSpy).toHaveBeenCalled();
    expect(scanSpy).toHaveBeenCalledWith(productCode);
  });

  it("should unscan product if DECREMENT_PRODUCT_QUANTITY is sent as action", function () {
    const productCode = "CAP";
    const unScanSpy = jest.fn();

    storeReducer({ checkout: { unScan: unScanSpy } }, { type: actionTypes.DECREMENT_PRODUCT_QUANTITY, productCode });

    expect(unScanSpy).toHaveBeenCalled();
    expect(unScanSpy).toHaveBeenCalledWith(productCode);
  });

  it("should change product quantity if CHANGE_PRODUCT_QUANTITY is sent as action", function () {
    const productCode = "CAP";
    const quantity = 3;
    const changeProductQuantitySpy = jest.fn();

    storeReducer(
      { checkout: { changeProductQuantity: changeProductQuantitySpy } },
      { type: actionTypes.CHANGE_PRODUCT_QUANTITY, productCode, quantity }
    );

    expect(changeProductQuantitySpy).toHaveBeenCalled();
    expect(changeProductQuantitySpy).toHaveBeenCalledWith(productCode, quantity);
  });

  it("should not modify state if action is not defined", function () {
    const oldState = { checkout: { fakeProperty: true } };

    const newState = storeReducer(oldState, { type: "NON_EXISTING_ONE" });

    expect(newState).toEqual(oldState);
  });
});
