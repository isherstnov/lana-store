import React, { useContext } from "react";
import { StoreContext } from "./store-context";
import StoreContextProvider from "./store-context";
import { render } from "@testing-library/react";

function FakeComponent() {
  const { checkout } = useContext(StoreContext);

  return checkout.products.map((product) => {
    return (
      <span key={product.code}>
        {product.code} quantity: {product.quantity}
      </span>
    );
  });
}

describe("store-context tests", function () {
  it("should initialize products to have quantity of 0", function () {
    const { getByText } = render(
      <StoreContextProvider>
        <FakeComponent />
      </StoreContextProvider>
    );

    expect(getByText("CAP quantity: 0")).toBeInTheDocument();
    expect(getByText("MUG quantity: 0")).toBeInTheDocument();
    expect(getByText("SHIRT quantity: 0")).toBeInTheDocument();
  });
});
