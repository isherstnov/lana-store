import * as actionTypes from "./store-actions";

const storeReducer = (state, action) => {
  const { productCode, quantity } = action;
  const { checkout } = state;

  switch (action.type) {
    case actionTypes.INCREMENT_PRODUCT_QUANTITY:
      checkout.scan(productCode);
      return {
        ...state,
        checkout,
      };
    case actionTypes.DECREMENT_PRODUCT_QUANTITY:
      checkout.unScan(productCode);
      return {
        ...state,
        checkout,
      };
    case actionTypes.CHANGE_PRODUCT_QUANTITY:
      checkout.changeProductQuantity(productCode, quantity);
      return {
        ...state,
        checkout,
      };
    default:
      return state;
  }
};

export default storeReducer;
