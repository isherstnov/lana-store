# Lana Store

## Introduction

The project is the result of the Take Home Test from Lana.

Made with `create-react-app` as React app boilerplate starting point, using React Context for maintaining state between components.

The project was started reusing the structure of the `html` file provided. Styling was reused as maximum as possible.

Styling was done with `_SCSS` to allow the re-usage of conventional spacing between elements, re-usage of colors used across the app and allowance of usage extra features in the styling such as nesting.

Testing was done with `react-testing-library`. Everything was covered with tests as much as possible.

## Considerations

- React used to easily keep updated the view with the data managed by JavaScript.
- React Context was used to spread the common state across the app.
- Products and discounts information was 'mocked' via JavaScript files located in `src/config` folder.
- Prettier used to format the code with a specific style among the whole app. Prettier is ran every time a commit is performed.
- There are two different types of components:
  1. `src/components`: Contains components that describe part of the React UI.
  2. `src/containers`: Contains containers that receive state updates and dispatch actions, and they usually don't render DOM elements.

## Running the app

1. Make sure you have installed NodeJS to be able to run the app. https://nodejs.org/
2. Run the command `npm install` in the root folder of the project to install all the dependencies.
3. To run the app execute `npm start` in the root folder of the project.
4. To run the tests execute `npm test` in the root folder of the project.
5. To build the app for production environment execute `npm run build`.

## Future improvements

- Implement internationalization to allow having different translations across the whole app for different languages. Deleting the hardcoded texts.
- If `src/assets` keep growing, consider using an S3 Bucket (or any other solution) to host these files.
- Consider using `styled-components` to give specific styles only scoping the proper component needed to style.
- Consider using `BEM` methodology solution for the naming convention.
- `Enzyme` is another option for the testing.
- Maybe is a good idea to make the app responsive, depending on the business needs.
